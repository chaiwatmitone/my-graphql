
Source code from tutorial
=======
https://engineering.thinknet.co.th/%E0%B8%AB%E0%B8%B1%E0%B8%94%E0%B9%80%E0%B8%82%E0%B8%B5%E0%B8%A2%E0%B8%99-api-service-%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-graphql-part-2-bdbb4eea9beb


how to
=========
- npm install
- yarn dev
- open http://localhost:4000/graphiql


create team
=============

mutation{
  createTeam(
    input: {
      team: {
        fullname: "Chelsea"
        shortname: "Chelsea"
        nickname: "Chelsea"
      }
    }
  ){
    meta {
      status
      message
    }
    data {
      _id
      fullname
      shortname
    }
  }
}

query
==========

query{
    teams(
      input: {
        keyword: "chelsea"
      }
  ){
    meta {
      status
      message
      total
      per_page
      current_page
      last_page
    }
    data {
      _id
      fullname
      shortname
    }
    errors {
        code
        message
    }
  }
}
